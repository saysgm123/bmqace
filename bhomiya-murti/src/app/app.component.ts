import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'marbal-idol';

  uemail = 'uname@bhomiyamurti.com';
  ucon = '+91 99999-99999';
}
